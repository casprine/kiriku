import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import {
  Nav,
  Navbar,
  NavDropdown,
  NavItem,
  MenuItem,
  Grid,
  Row,
  Col
} from "react-bootstrap";
import UserInitialsCircle from "../../misc/UserInitialsCircle";
import CollectionsIcon from "react-icons/lib/ti/book";
import NewPostIcon from "react-icons/lib/ti/edit";
import LogoutIcon from "react-icons/lib/ti/chevron-left-outline";

import Dashboard from "./Dashboard/Container";
import Collections from "./Collections";
import "./style.css";

const Admin = props => {
  const { firebase, auth } = props;
  const userEmail = auth.email;
  const Npost = "New Post";

  return (
    <div className="LunaAdmin">
      <div className="header">
        <Navbar fixedTop collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/admin">Kiriku</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav pullRight>
              <NavItem className="new-post">
                <Link to="/admin/collections/article/new">
                  <NewPostIcon />
                </Link>
              </NavItem>

              <NavDropdown
                eventKey={3}
                title={<UserInitialsCircle userName={userEmail} />}
                id="basic-nav-dropdown"
              >
                <MenuItem header>{userEmail}</MenuItem>
                <MenuItem divider />
                <MenuItem
                  eventKey={3.3}
                  onSelect={() => {
                    firebase.logout();
                  }}
                >
                  <LogoutIcon /> Logout
                </MenuItem>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>

      <div className="LunaAdmin-Content">
        <Grid>
          <Switch>
            <Route exact path="/admin" component={Dashboard} />
            <Route path="/admin/collections" component={Collections} />
          </Switch>
        </Grid>
      </div>
    </div>
  );
};

export default Admin;
